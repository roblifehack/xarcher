/*
 * Thanks to https://gamemechanicexplorer.com/#bullets-4
 */

var xArcher = xArcher || {};

xArcher.play = function() {};

xArcher.play.prototype = {
    preload: function() {
        this.game.time.advancedTiming = true;
        xArcher.score = 0;
        this.power = 600;
        this.misses = 0;
        this.target = null;
    },

    create: function() {
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
        this.textLabel = this.game.add.text(30, 30, 'Score: 0   Angle: 0   Power: 600   Misses: 0',
            { font: '18px serif', fill: '#ffffff'});
        this.player = this.game.add.sprite(20, this.game.height - 64, 'figure');
        this.bow = new xArcher.Bow(this.game, 'bow', this.player, this);     //this.game.add.sprite(this.player.position.x + 16, this.player.position.y + 16, 'bow');

        this.game.physics.arcade.gravity.y = 980;
        /*
        this.ground = this.add.group();
        for(var x=0; x<this.game.width; x+=32) {
            var groundBlock = this.game.add.sprite(x, this.game.height-32, 'ground');
            this.game.physics.enable(groundBlock, Phaser.Physics.ARCADE);
            groundBlock.body.immovable = true;
            groundBlock.body.allowGravity = false;
            this.ground.add(groundBlock);
        }
        */
        this.createGround();
        this.game.add.existing(this.bow);
        this.target = new xArcher.Target(this.game, 'target');
        this.target.x = 400;
        this.target.y = this.game.height - 32 - 8;
        this.target.allowGravity = false;
        this.game.add.existing(this.target);
    },
    
    createGround: function() {
        this.ground = this.add.group();
        for(var x=0; x<this.game.width; x+=32) {
            var groundBlock = this.game.add.sprite(x, this.game.height-32, 'ground');
            this.game.physics.enable(groundBlock, Phaser.Physics.ARCADE);
            groundBlock.body.immovable = true;
            groundBlock.body.allowGravity = false;
            this.ground.add(groundBlock);
        }
        for(var x=150; x<350; x+=32) {
            var groundBlock = this.game.add.sprite(x, this.game.height - 232, 'ground');
            this.game.physics.enable(groundBlock, Phaser.Physics.ARCADE);
            groundBlock.body.immovable = true;
            groundBlock.body.allowGravity = false;
            this.ground.add(groundBlock);
        }
    },
    
    update: function() {
        this.textLabel.text = 'Score: ' + xArcher.score + '   Angle:' + this.bow.angle * -1 + '   Power: ' + this.bow.power + '   Misses: ' + this.misses;
        //physics check for target
        
        if(this.game.physics.arcade.collide(this.bow.arrow, this.ground)) {
            this.misses++;
            if(this.misses >= 10) {
                this.game.state.start('gameover');
            }
            this.bow.arrow.kill();
        }
        
        if(this.game.physics.arcade.collide(this.bow.arrow, this.target)) {
            this.bow.arrow.kill();
            xArcher.score += 10;
            this.targetHit();
        }
        
        if(this.cursors.up.isDown) {
            this.bow.angle -=2;
        }
        
        if(this.cursors.right.isDown) {
            this.bow.power +=5;
        }

        if(this.cursors.left.isDown) {
            this.bow.power -= 5;
        }

        if(this.cursors.down.isDown) {
            this.bow.angle += 2;
        }
        if(this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.bow.fire();
        }
    },
    
    targetHit: function() {
        var x = this.game.rnd.integerInRange(150, 450);
        var y = 0;
        if( x > 150 && x < 350) {
            y = this.game.height - 232 - 8;
        } else if( x > 350 && x< 450) {
            y = this.game.height - 32 - 8;
        } else {
            x = 450;
            y = this.game.height - 32 - 8;
        }
        this.target.kill();
        console.log('x: ' + x + 'y: ' + y);
        this.target = new xArcher.Target(this.game, 'target');
        this.target.x = x;
        this.target.y = y;
        this.target.immovable = true;
        this.game.add.existing(this.target);
    }
    
};
