var xArcher = xArcher || {};

xArcher.gameover = function() {};

xArcher.gameover.prototype = {
    update: function() {
        this.game.world.removeAll();
        this.game.add.text(200, 200, 'Game Over Score is: ' + xArcher.score,
            {font: '22px serif', fill: '#000000'});
    }
};