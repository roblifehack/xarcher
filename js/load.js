var xArcher = xArcher || {};

xArcher.load = function(){};

xArcher.load.prototype = {
    preload: function() {
        this.game.load.image('ground', 'assets/images/ground.png');
        this.game.load.image('figure', 'assets/images/figure.png');
        this.game.load.image('bow', 'assets/images/bow.png');
        this.game.load.image('arrow', 'assets/images/projectile.png');
        this.game.load.image('target', 'assets/images/target.png');
    },
    create: function() {
        this.game.state.start('play');
    }
};