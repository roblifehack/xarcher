var xArcher = xArcher || {};

xArcher.Target = function(game, key) {
  Phaser.Sprite.call(this, game, 0, 0, key);
  this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
  this.anchor.setTo(0.5, 0.5);
  
  this.game.physics.enable(this, Phaser.Physics.ARCADE);
  this.body.allowGravity = false;
};

xArcher.Target.prototype = Object.create(Phaser.Sprite.prototype);
xArcher.Target.prototype.constructor = xArcher.Target;