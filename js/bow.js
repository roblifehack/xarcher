var xArcher = xArcher || {};

xArcher.Bow = function(game, key, player, state) {
    Phaser.Sprite.call(this, game, player.position.x + 16, 
        player.position.y + 16, key);
    this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
    this.anchor.setTo(0, 0.5);
    this.rotation = 0;
    this.power = 400;
    
    this.arrow = this.game.add.sprite(0, 0, 'arrow');
    this.arrow.anchor.setTo(0.5, 0.5);
    this.game.physics.enable(this.arrow, Phaser.Physics.ARCADE);
    this.arrow.kill();
    /*
    this.projectilePool = state.add.group();
    var projectile = this.game.add.sprite(1, 1, 'arrow');
    projectile.anchor.setTo(0.5, 0.5);
    this.game.physics.enable(projectile, Phaser.Physics.ARCADE);
    projectile.kill();
    this.projectilePool.add(projectile);
    */
};

xArcher.Bow.prototype = Object.create(Phaser.Sprite.prototype);
xArcher.Bow.prototype.constructor = xArcher.Bow;

xArcher.Bow.setAnchor = function(x, y) {
    this.setAnchor(x, y);
};

xArcher.Bow.prototype.rotationUp = function() {
    this.rotation -= 2;
};

xArcher.Bow.prototype.rotationDown = function() {
    this.rotation += 2;
};

xArcher.Bow.prototype.powerUp = function() {
    this.power += 5;
};

xArcher.Bow.prototype.powerDown = function() {
    this.power -= 5;
};

xArcher.Bow.prototype.getRotation = function() {
    return this.rotation;
};

xArcher.Bow.prototype.getPower = function() {
    return this.power;
};

xArcher.Bow.prototype.fire = function() {
    this.arrow.revive();
    
    this.arrow.checkWorldBounds = true;
    this.arrow.outOfBoundsKill = true;
    this.arrow.reset(this.x, this.y);
    this.arrow.rotation = this.rotation;

    this.arrow.body.velocity.x = Math.cos(this.arrow.rotation) * this.power;
    this.arrow.body.velocity.y = Math.sin(this.arrow.rotation) * this.power;
};